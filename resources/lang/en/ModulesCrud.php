<?php

return [
    'module_singular' => 'Module',
    'module_plural' => 'Modules',
    'name' => 'Nombre',
    'vendor' => 'Vendor',
    'starter' => 'Base',
    'url' => 'Url',
    'scaffold' => 'Scaffold',
    'download' => 'Download',
    'back' => 'Back',
    'non_existant_starter' => 'Starter codebase does not exist',

    'starters' => [
        'dummy-crud' => [
            'vendor' => 'Vendor',
            'name' => 'Name',
            'icon' => 'Icon',
            'model' => 'Model',
            'model_plural' => 'Model plural',
            'route_prefix' => 'Route prefix',
            'tenant' => 'Tenant',
            'table_name' => 'Table name',
        ]
    ]
];
