<?php

return [
    'module_singular' => 'Módulo',
    'module_plural' => 'Módulos',
    'name' => 'Nombre',
    'vendor' => 'Vendor',
    'starter' => 'Base',
    'url' => 'Url',
    'scaffold' => 'Generar',
    'download' => 'Descargar',
    'back' => 'Volver',
    'non_existant_starter' => 'Projecto base no existe',

    'starters' => [
        'dummy-crud' => [
            'vendor' => 'Vendor',
            'name' => 'Nombre',
            'icon' => 'Icono',
            'model' => 'Modelo',
            'model_plural' => 'Plural del modelo',
            'route_prefix' => 'Prefijo de ruta',
            'tenant' => 'Tenant',
            'table_name' => 'Nombre de tabla',
        ]
    ]
];
