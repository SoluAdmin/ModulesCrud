@extends('backpack::layout')

@section('header')
    <section class="content-header">
        <h1>
            {{ trans('SoluAdmin::ModulesCrud.scaffold') }} <span class="text-lowercase">{{ $crud->entity_name }}</span>
        </h1>
        <ol class="breadcrumb">
            <li>
                <a href="{{ url(config('backpack.base.route_prefix'), 'dashboard') }}">{{ trans('backpack::crud.admin') }}</a>
            </li>
            <li><a href="{{ url($crud->route) }}" class="text-capitalize">{{ $crud->entity_name_plural }}</a></li>
            <li class="active">{{ trans('SoluAdmin::ModulesCrud.scaffold') }}</li>
        </ol>
    </section>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <!-- Default box -->
            @if ($crud->hasAccess('list'))
                <a href="{{ url($crud->route) }}"><i
                            class="fa fa-angle-double-left"></i> {{ trans('backpack::crud.back_to_all') }} <span
                            class="text-lowercase">{{ $crud->entity_name_plural }}</span></a><br><br>
            @endif

            @include('crud::inc.grouped_errors')

            {!! Form::open(['url' => $crud->route.'/'.$entry->getKey(). '/scaffold', 'method' => 'post' ]) !!}
            <div class="box">

                <div class="box-header with-border">
                    <h3 class="box-title">{{ trans('SoluAdmin::ModulesCrud.scaffold') }} {{ $crud->entity_name }}</h3>
                </div>
                <div class="box-body row">
                    <!-- load the view from the application if it exists, otherwise load the one in the package -->
                    @if(view()->exists('vendor.backpack.crud.form_content'))
                        @include('vendor.backpack.crud.form_content', [ 'fields' => $fields, 'action' => 'create' ])
                    @else
                        @include('crud::form_content', [ 'fields' => $fields, 'action' => 'create' ])
                    @endif
                </div><!-- /.box-body -->
                <div class="box-footer">
                    <div id="saveActions" class="form-group">
                        <button type="submit" class="btn btn-success">
                            <span class="fa fa-download" role="presentation" aria-hidden="true"></span> &nbsp;
                            <span>{{ trans('SoluAdmin::ModulesCrud.download') }}</span>
                        </button>
                        <a href="{{ url($crud->route) }}" class="btn btn-default"><span class="fa fa-arrow-left"></span>
                            &nbsp;{{ trans('SoluAdmin::ModulesCrud.back') }}</a>
                    </div>
                </div><!-- /.box-footer-->
            </div><!-- /.box -->
            {!! Form::close() !!}
        </div>
    </div>

@endsection