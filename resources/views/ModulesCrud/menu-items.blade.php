@module("SoluAdmin\\ModulesCrud")
<li>
    <a href="{{ url(config('backpack.base.route_prefix', 'admin') . config('SoluAdmin.ModulesCrud.route_prefix', '') . '/module') }}">
        <i class="fa fa-wrench"></i> <span>{{trans('SoluAdmin::ModulesCrud.module_plural')}}</span>
    </a>
</li>
@endmodule