@if ($entry->starter)
    <a href="{{ url($crud->route.'/'.$entry->getKey(). '/scaffold') }}"
       class="btn btn-xs btn-default">
        <i class="fa fa-code"></i> {{ trans('SoluAdmin::ModulesCrud.scaffold') }}
    </a>
@endif