<?php

namespace SoluAdmin\ModulesCrud\Providers;

use Backpack\CRUD\CrudServiceProvider as CRUDRouter;
use Illuminate\Routing\Router;
use Illuminate\View\Compilers\BladeCompiler;
use SoluAdmin\ModulesCrud\ModuleGenerator\ModuleGenerator;
use SoluAdmin\Support\Providers\CrudServiceProvider;
use SoluAdmin\Support\Helpers\PublishableAssets as Assets;

class ModulesCrudServiceProvider extends CrudServiceProvider
{
    protected $assets = [
        Assets::CONFIGS,
        Assets::TRANSLATIONS,
        Assets::VIEWS,
        Assets::MIGRATIONS,
    ];

    protected $resources = [
        'module' => 'ModuleCrudController'
    ];

    protected function registerExtras()
    {
        $this->setupExtraRoutes($this->app->router);
        $this->registerDependencies();
        $this->registerBladeExtensions();

        $this->app->bind(ModuleGenerator::class, function () {
            return new ModuleGenerator;
        });
    }

    protected function bootExtras()
    {
        $this->publishes([__DIR__ . '/../../resources/extras/views' => base_path('resources/views')], 'views');
    }

    private function setupExtraRoutes(Router $router)
    {
        $router->group(
            ['namespace' => "SoluAdmin\\ModulesCrud\\Http\\Controllers"],
            function () use ($router) {
                $router->group([
                    'prefix' => config('backpack.base.route_prefix', 'admin')
                        . config('SoluAdmin.ModulesCrud.route_prefix', ''),
                    'middleware' => config("SoluAdmin.ModulesCrud.middleware")
                        ? array_merge(['web', 'admin'], config("SoluAdmin.ModulesCrud.middleware"))
                        : ['web', 'admin'],
                ], function () use ($router) {
                    CRUDRouter::resource('module', 'ModuleCrudController')->with(
                        function () use ($router) {
                            $router->get('module/{module}/scaffold', 'ModuleCrudController@showScaffoldForm')
                                ->name('module.show-scaffold-form');
                            $router->post('module/{module}/scaffold', 'ModuleCrudController@scaffold')
                                ->name('module.scaffold');
                        }
                    );
                });
            }
        );
    }

    private function registerDependencies()
    {
        $this->app->register(\Chumper\Zipper\ZipperServiceProvider::class);

        $loader = \Illuminate\Foundation\AliasLoader::getInstance();
        $loader->alias('Zipper', \Chumper\Zipper\Zipper::class);
    }

    private function registerBladeExtensions()
    {
        $this->app->afterResolving('blade.compiler', function (BladeCompiler $bladeCompiler) {
            $bladeCompiler->directive('module', function ($module) {
                return "<?php if(auth()->check() && auth()->user()->hasModule({$module})): ?>";
            });
            $bladeCompiler->directive('endmodule', function () {
                return '<?php endif; ?>';
            });
        });
    }
}
