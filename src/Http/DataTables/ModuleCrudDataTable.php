<?php

namespace SoluAdmin\ModulesCrud\Http\DataTables;

use SoluAdmin\Support\Interfaces\DataTable;

class ModuleCrudDataTable implements DataTable
{
    public function columns()
    {
        return [
            [
                'name' => 'vendor',
                'label' => trans('SoluAdmin::ModulesCrud.vendor'),
            ],
            [
                'name' => 'name',
                'label' => trans('SoluAdmin::ModulesCrud.name'),
            ],
            [
                'name' => 'starter',
                'label' => trans('SoluAdmin::ModulesCrud.starter'),
            ]
        ];
    }
}
