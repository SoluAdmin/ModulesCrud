<?php

namespace SoluAdmin\ModulesCrud\Http\Forms;

use SoluAdmin\ModulesCrud\ModuleGenerator\ModuleGenerator;
use SoluAdmin\Support\Interfaces\Form;

class ModuleCrudForm implements Form
{
    public function __construct()
    {
        $this->generator = app(ModuleGenerator::class);
    }

    public function fields()
    {
        return [
            [
                'name' => 'vendor',
                'label' => trans('SoluAdmin::ModulesCrud.vendor'),
            ],
            [
                'name' => 'name',
                'label' => trans('SoluAdmin::ModulesCrud.name'),
            ],
            [
                'name' => 'starter',
                'label' => trans('SoluAdmin::ModulesCrud.starter'),
                'type' => 'select_from_array',
                'options' => $this->generator->getStarters(),
                'allows_null' => true,
            ],
        ];
    }

    public function getStarters()
    {
        return $this->generator->getStarters();
    }

    public function getPlaceholderFields($module)
    {
        return $this->generator->getPlaceholderFields($module);
    }
}
