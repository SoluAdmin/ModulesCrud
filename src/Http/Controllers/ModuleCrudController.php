<?php

namespace SoluAdmin\ModulesCrud\Http\Controllers;

use Illuminate\Http\Request;
use SoluAdmin\ModulesCrud\Http\Requests\ModuleCrudRequest as StoreRequest;
use SoluAdmin\ModulesCrud\Http\Requests\ModuleCrudRequest as UpdateRequest;
use SoluAdmin\ModulesCrud\Models\Module;
use SoluAdmin\ModulesCrud\ModuleGenerator\ModuleGenerator;
use SoluAdmin\Support\Http\Controllers\BaseCrudController;

class ModuleCrudController extends BaseCrudController
{
    protected $generator;

    public function __construct(ModuleGenerator $generator)
    {
        $this->generator = $generator;
        parent::__construct();
    }

    public function setup()
    {
        parent::setup();

        $this->crud->addButtonFromView('line', 'scaffold', 'scaffold', 'scaffold');
        $this->crud->enableReorder('namespace', 1);
        $this->crud->allowAccess('reorder');
    }

    public function showScaffoldForm(Module $module)
    {
        $this->starterExistsOrFail($module->starter);

        $this->data['crud'] = $this->crud;
        $this->data['fields'] = $this->form()->getPlaceholderFields($module);
        $this->data['title'] = trans('SoluAdmin::ModuleCrud.scaffold') . ' ' . $this->crud->entity_name;
        $this->data['entry'] = $module;

        return view('SoluAdmin::ModulesCrud.scaffold', $this->data);
    }

    public function scaffold(Module $module, Request $request)
    {
        $this->starterExistsOrFail($module->starter);
        $path = storage_path() . "/zips/" . str_slug($module->name) . ".zip";
        $this->generator->generateModuleZip($path, $module->starter, $request->except('_token'));
        return response()->download($path)->deleteFileAfterSend(true);
    }

    public function store(StoreRequest $request)
    {
        return parent::storeCrud();
    }

    public function update(UpdateRequest $request)
    {
        return parent::updateCrud();
    }

    private function starterExistsOrFail($starter)
    {
        if (!in_array($starter, $this->form()->getStarters())) {
            abort(405, trans('SoluAdmin::ModulesCrud.non_existant_starter'));
        };
    }
}
