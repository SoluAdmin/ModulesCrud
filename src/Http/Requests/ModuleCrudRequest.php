<?php

namespace SoluAdmin\ModulesCrud\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ModuleCrudRequest extends FormRequest
{

    public function authorize()
    {
        return Auth::check();
    }

    public function rules(Request $request)
    {
        return [
            'vendor' => 'required|unique_with:modules,name,' . $request->get('id'),
            'name' => 'required',
        ];
    }
}
