<?php

namespace SoluAdmin\ModulesCrud\Models;

use Backpack\CRUD\CrudTrait;
use Illuminate\Database\Eloquent\Model;

class Module extends Model
{
    use CrudTrait;

    protected $guarded = ['id'];

    public function __construct(array $attributes = [])
    {
        $this->connection = config('SoluAdmin.ModulesCrud.connection');
        parent::__construct($attributes);
    }

    public function getNamespaceAttribute()
    {
        return "{$this->vendor}\\{$this->name}";
    }

    public function users()
    {
        return $this->belongsToMany(
            config('SoluAdmin.ModulesCrud.user_model'),
            'module_users'
        );
    }

    public function tenants()
    {
        return $this->belongsToMany(
            config('SoluAdmin.ModulesCrud.tenant_model'),
            'module_tenants'
        );
    }
}
