<?php

return [
    'route_prefix' => '{:route_prefix}',
    'middleware' => false, // If we need to secure it we use middlewares, ex: ['can:do-anything']
    'setup_routes' => false,
    'publishes_migrations' => true,
];
