<?php

namespace {:vendor}\{:name}\Providers;

use SoluAdmin\Support\Providers\CrudServiceProvider;
use SoluAdmin\Support\Helpers\PublishableAssets as Assets;

class {:name}ServiceProvider extends CrudServiceProvider
{
    protected $assets = [
        Assets::CONFIGS,
        Assets::SEEDS,
        Assets::TRANSLATIONS,
        Assets::VIEWS,
        Assets::MIGRATIONS,
        Assets::MODULE_MIGRATIONS,
    ];

    protected $resources = [
        '{:model|str_slug}' => '{:model}CrudController'
    ];
}
