<?php

namespace {:vendor}\{:name}\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class {:model}CrudRequest extends FormRequest
{

    public function authorize()
    {
        return Auth::check();
    }

    public function rules()
    {
        return [
          'name' => 'required',
        ];
    }
}
