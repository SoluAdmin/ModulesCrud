<?php

namespace {:vendor}\{:name}\Http\DataTables;

use SoluAdmin\Support\Interfaces\DataTable;

class {:model}CrudDataTable implements DataTable
{
    public function columns()
    {
        return [
            [
                'name' => 'name',
                'label' => trans('{:vendor}::{:name}.name'),
            ],
        ];
    }
}
