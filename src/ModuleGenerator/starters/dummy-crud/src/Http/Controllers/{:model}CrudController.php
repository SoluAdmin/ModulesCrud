<?php

namespace {:vendor}\{:name}\Http\Controllers;

use SoluAdmin\Support\Http\Controllers\BaseCrudController;
use {:vendor}\{:name}\Http\Requests\{:model}CrudRequest as StoreRequest;
use {:vendor}\{:name}\Http\Requests\{:model}CrudRequest as UpdateRequest;
use {:vendor}\{:name}\Models\{:model};

class {:model}CrudController extends BaseCrudController
{

    public function setup()
    {
        parent::setup();
    }

    public function store(StoreRequest $request)
    {
        return parent::storeCrud();
    }

    public function update(UpdateRequest $request)
    {
        return parent::updateCrud();
    }
}
