<?php

namespace {:vendor}\{:name}\Http\Forms;

use SoluAdmin\Support\Interfaces\Form;

class {:model}CrudForm implements Form
{
    public function fields()
    {
        return [
            [
                'name' => 'name',
                'label' => trans('{:vendor}::{:name}.name'),
            ],
        ];
    }
}
