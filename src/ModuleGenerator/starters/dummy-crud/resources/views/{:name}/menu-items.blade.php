@if(\Auth::user()->hasModule('{:vendor}\\{:name}'))
<li>
    <a href="{{ url(config('backpack.base.route_prefix', 'admin') . config('{:vendor}.{:name}.route_prefix', '') . '/{:model|str_slug}') }}">
        <i class="fa {:icon}"></i> <span>{{trans('{:vendor}::{:name}.{:model|snake_case}_plural')}}</span>
    </a>
</li>
@endif