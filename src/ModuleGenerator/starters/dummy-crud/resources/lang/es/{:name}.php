<?php

return [
    '{:model|snake_case}_singular' => '{:model}',
    '{:model|snake_case}_plural' => '{:model_plural}',
];
