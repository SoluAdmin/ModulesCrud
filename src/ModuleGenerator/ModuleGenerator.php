<?php

namespace SoluAdmin\ModulesCrud\ModuleGenerator;

use Illuminate\Support\Collection;
use Illuminate\Support\Str;
use RecursiveIteratorIterator;
use RegexIterator;
use RecursiveDirectoryIterator;
use SoluAdmin\ModulesCrud\Models\Module;

class ModuleGenerator
{
    const PLACEHOLDER_REGEX = '/{:[^}]*[}]/';
    const NAME_REGEX = '{:([^:\}|]*)}';
    const FUNCTION_REGEX = '/\|(.*)}/';

    public function getStarters()
    {
        return array_reduce(glob(__DIR__ . '/starters/*'), function ($carry, $item) {
            $name = basename($item);
            $carry[$name] = $name;
            return $carry;
        }, []);
    }

    public function getPlaceholderFields(Module $module)
    {
        return $this->getPlaceholderVariables($module->starter)->map(function ($item) use ($module) {
            $key = "ModulesCrud.starters.{$module->starter}.{$item}";

            return [
                'name' => $item,
                'type' => config("SoluAdmin.{$key}", 'text'),
                'label' => \Lang::has("SoluAdmin::{$key}") ? trans("SoluAdmin::{$key}") : $item,
                'value' => $module->{$item} ? $module->{$item} : '',
                'attributes' => [
                    'required' => 'required',
                ],
            ];
        });
    }

    public function generateModuleZip($path, $starter, $inputs)
    {
        $moduleFolder = substr(basename($path), 0, -4);
        $files = $this->replacePlaceholders($starter, $inputs);

        /** @var Zipper $zip */
        $zip = \Zipper::make($path)->folder($moduleFolder);

        $files->each(function ($content, $filename) use ($zip) {
            $zip->addString($filename, $content);
        });

        $zip->close();
    }

    private function replacePlaceholders($starter, $inputs)
    {
        /** @var Collection $replaces */
        $replaces = $this->getPlaceholders($starter)->reduce(function (Collection $carry, $placeholder) use ($inputs) {
            $replace = $inputs[$this->extractNameFromPlaceholder($placeholder)];
            $value = (Str::contains($placeholder, '|'))
                ? $this->applyFunctionFromPlaceholder($placeholder, $replace)
                : $replace;
            $carry->put($placeholder, $value);
            return $carry;
        }, collect());

        return $this->getStarterFiles($starter)->reduce(
            function (Collection $carry, $file) use ($starter, $replaces) {
                $replacedFile = $this->applyReplacesToFile($starter, $file, $replaces);

                $carry->put($replacedFile['file'], $replacedFile['content']);
                return $carry;
            },
            collect()
        );
    }

    private function applyReplacesToFile($starter, $file, $replaces)
    {
        $fileName = str_replace(__DIR__ . "/starters/$starter/", '', $file);
        $contents = file_get_contents($file);

        foreach ($replaces as $key => $value) {
            $contents = str_replace($key, $value, $contents);
            $fileName = str_replace($key, $value, $fileName);
        }

        return ['file' => $fileName, 'content' => $contents];
    }

    /**
     * @param $starter
     * @return Collection
     */
    private function getPlaceholders($starter)
    {
        $files = $this->getStarterFiles($starter);

        return $files
            ->reduce(function (Collection $carry, $file) {
                $filePlaceholders = $this->getPlaceholdersFromFile($file);

                return $carry->merge($filePlaceholders);
            }, collect())
            ->unique();
    }

    private function getPlaceholderVariables($starter)
    {
        return $this->getPlaceholders($starter)
            ->map(function ($placeholder) {
                return $this->extractNameFromPlaceholder($placeholder);
            })
            ->unique();
    }

    private function applyFunctionFromPlaceholder($placeholder, $subject)
    {
        $function = $this->extractFunctionFromPlaceholder($placeholder);
        return $function($subject);
    }

    private function extractNameFromPlaceholder($placeholder)
    {
        $matches = [];
        preg_match_all(static::NAME_REGEX, $placeholder, $matches);
        return $matches[1][0];
    }

    private function extractFunctionFromPlaceholder($placeholder)
    {
        $matches = [];
        preg_match_all(static::FUNCTION_REGEX, $placeholder, $matches);
        return $matches[1][0];
    }

    private function getPlaceholdersFromFile($file)
    {
        $fileNamePlaceholders = $this->getPlaceholdersFromSubject($file);
        $fileContentPlaceholders = $this->getPlaceholdersFromSubject(file_get_contents($file));

        return $fileContentPlaceholders->merge($fileNamePlaceholders);
    }

    private function getPlaceholdersFromSubject($subject)
    {
        $matches = [];
        preg_match_all(static::PLACEHOLDER_REGEX, $subject, $matches);
        return collect($matches)->flatten();
    }

    private function getStarterFiles($starter)
    {
        $dir = new RecursiveDirectoryIterator(__DIR__ . "/starters/$starter/", RecursiveDirectoryIterator::SKIP_DOTS);
        $ite = new RecursiveIteratorIterator($dir);
        $files = new RegexIterator($ite, "/.*/", RegexIterator::GET_MATCH);

        return collect($files)->values()->flatten();
    }
}
