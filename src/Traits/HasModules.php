<?php

namespace SoluAdmin\ModulesCrud\Traits;

use SoluAdmin\ModulesCrud\Models\Module;

trait HasModules
{
    abstract public function getModulesPivotTable();

    public function modules()
    {
        return $this->belongsToMany(
            Module::class,
            $this->getModulesPivotTable()
        );
    }

    public function hasModule($module)
    {
        if (is_string($module)) {
            return $this->modules->contains('namespace', $module);
        }

        if ($module instanceof Module) {
            return $this->modules->contains('id', $module->id);
        }

        return (bool) $module->intersect($this->modules)->count();
    }
}
