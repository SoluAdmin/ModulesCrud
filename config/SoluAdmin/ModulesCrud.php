<?php

return [
    'route_prefix' => '',
    'middleware' => false,
    'user_model' => 'App\Models\Permission\User',
    'tenant_model' => 'App\Models\Tenants\Tenant',

    'connection' => 'authorization',
    'setup_routes' => false,
    'publishes_migrations' => false,

    'starters' => [
        'dummy-crud' => [
            'icon' => 'icon_picker',
        ]
    ]
];
